provider "aws" {
  region = "eu-central-1"
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  version = "5.7.1"

  name = "${var.name_cluster}-vpc"
  cidr = var.vpc_cidr

  azs             = data.aws_availability_zones.azs.names
  private_subnets = var.private_cidr_bloc
  public_subnets  = var.public_cidr_bloc

  enable_nat_gateway = true
  single_nat_gateway = true
  enable_dns_hostnames = true

  tags = {
    "kubernetes.io/cluster/${var.name_cluster}" = "shared"
  }

  public_subnet_tags = {
     "kubernetes.io/cluster/${var.name_cluster}" = "shared"
     "kubernetes.io/role/elb" = 1
  }

  private_subnet_tags = {
     "kubernetes.io/cluster/${var.name_cluster}" = "shared"
     "kubernetes.io/role/internal-elb" = 1
  }
}